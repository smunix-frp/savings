module Savings.Clock.Daily ( Daily
                           , Money
                           , depositsStore
                           , interestsStore
                           , moneyS
                           , interestS
                           , depositS
                           , clock
                           , ioPrint
                           , projectClient
                           , test
                           , HasDay(..)
                           , HasDeposit(..)
                           , HasDeposits(..)
                           , type MoneySF
                           ) where

-- rhine
import FRP.Rhine ( Clock(..)
                 , ClSF
                 , count
                 , MonadIO(..)
                 , arrMCl
                 , flow
                 , tagS
                 , (@@)
                 )
import FRP.Rhine.Clock ( Clock(..)
                       )
import FRP.Rhine.Clock.Realtime.Millisecond ( Millisecond
                                            , waitClock
                                            )
import FRP.Rhine.Clock.Realtime.Stdin ( StdinClock(..)
                                      )

import FRP.Rhine.Clock.Realtime.Busy ( Busy(..)
                                      )

-- dunai
import Data.MonadicStreamFunction ( feedback
                                  )
-- base
import Control.Arrow
import Control.Monad ( guard
                     , when
                     )
import Data.Proxy
import GHC.TypeLits
import Data.Coerce (coerce)
import Data.Foldable (foldMap)
import Data.Monoid ( Sum(..)
                   , Product(..)
                   )

-- generics
import GHC.Generics (Generic)

-- hashable
import Data.Hashable

-- hashmap
import qualified Data.HashMap.Lazy as HM

-- containers
import qualified Data.Sequence as S

-- time
import Data.Time.Calendar
import Data.Time.Format
-- import Data.Time.Clock

-- | A clock that computes and publishes current daily of the simulation
data Daily cl where
  Daily :: { simClock :: cl
           , simStart :: Day
           } -> Daily cl

class HasClock cl where
  clock :: cl -> Day -> Daily cl

instance HasClock (Daily cl) where
  clock dc@Daily{ simClock = cl, .. } d = Daily { simClock = dc{ simStart = d}
                                                , simStart = d
                                                }

instance (KnownNat n) => HasClock (Millisecond n) where
  clock = Daily

instance HasClock StdinClock where
  clock = Daily

instance HasClock Busy where
  clock = Daily

instance forall m cl . (Monad m, Clock m cl) => Clock m (Daily cl) where
  type Time (Daily cl) = Time cl
  type Tag (Daily cl) = Day
  initClock Daily {..} = do
    (runningClock, initialTime) <- initClock simClock
    let
      -- runningDaily :: forall m cl . MSF m () (Time cl, Day)
      runningDaily = feedback (simStart) (proc (_, i) -> do
                                   (time, _) <- runningClock -< ()
                                   returnA -< ((time, i), addDays 1 i)
                               )
    return (runningDaily, initialTime)

-- | compound interests (at specific dailys of the year)
compound :: (MonadIO m) => m ()
compound = liftIO $ print ("compounding interests")
-- | daily abstracted action function
act :: (MonadIO m) => Day -> (Integer, Int, Int) -> m ()
act day (yyyy, mm, dd) = liftIO $ print day

ioPrint :: (MonadIO m) => Day -> m ()
ioPrint d@(toGregorian -> gd@(_,_,15)) = compound >> act d gd
ioPrint d@(toGregorian -> gd@(_,_,_)) = act d gd

-- | parse time
pt :: [Char] -> Maybe Day
pt (parseTimeM @Maybe @Day True defaultTimeLocale "%Y-%-m-%-d" -> d@(Just _)) = d
pt (parseTimeM @Maybe @Day True defaultTimeLocale "%Y %-m %-d" -> d@(Just _)) = d
pt _ = Nothing

type Freq = Integer

data DayH where
  DayH :: { yyyy :: Integer
          , mm :: Int
          , dd :: Int
          } -> DayH
  deriving (Show, Eq, Ord, Generic)
instance Hashable DayH

class HasDay a where
  day :: a -> DayH
  calDay :: a -> Day

instance HasDay Day where
  day d = DayH {..}
    where
      (yyyy, mm, dd) = toGregorian d
  calDay = id

instance HasDay DayH where
  day = id
  calDay DayH{..} = fromGregorian yyyy mm dd

instance HasDay [Char] where
  day (pt -> Just d) = day d
  calDay (pt -> Just d) = d

instance (yyyy ~ Integer, mm ~ Int, dd ~ Int) => HasDay (yyyy, mm, dd) where
  day (yyyy, mm, dd) = DayH yyyy mm dd
  calDay = calDay . day

-- | money type alias
newtype Money = Money Double
  deriving (Show, Eq, Ord, Num, Fractional, Real)

instance Semigroup Money where
  m1 <> m2 = coerce (Sum m1 <> Sum m2)

instance Monoid Money where
  mempty = coerce (mempty :: Sum Money)
  mappend = (<>)

class HasMoney a where
  money :: a -> Money

instance HasMoney Money where
  money = id

newtype Rate = Rate Double
  deriving (Show, Eq, Ord, Num, Fractional, Real)

class HasRate a where
  rate :: a -> Rate

-- | a single deposit (no day)
newtype Deposit = Deposit { unDeposit :: S.Seq Money }
  deriving (Show, Eq)

instance HasMoney Deposit where
  money = getSum . foldMap (Sum . money) . unDeposit

-- | a single interest (no day)
newtype Interest = Interest { unInterest :: S.Seq Rate }
  deriving (Show, Eq)

instance HasRate Interest where
  rate (S.viewr . coerce -> (_ S.:> r)) = r
  rate (S.viewr . coerce -> (S.EmptyR :: S.ViewR Rate)) = (rate (mempty :: Interest))

instance Semigroup Interest where
  i1 <> i2 = Interest $ (coerce i1) S.>< (coerce i2)

instance Monoid Interest where
  mempty = Interest $ S.singleton 0.0
  mappend = (<>)

class HasInterest a where
  interest :: a -> Interest

instance HasInterest Interest where
  interest = id

instance HasInterest Rate where
  interest = Interest . S.singleton
class HasDeposit a where
  deposit :: a -> Deposit

instance HasDeposit Money where
  deposit money = Deposit (S.singleton money)

instance Semigroup Deposit where
  d1 <> d2 = Deposit $ (coerce d1) S.>< (coerce d2)

instance Monoid Deposit where
  mempty = Deposit (S.empty)
  mappend = (<>)

-- | SearchFor
class SearchFor c where
  type family K c :: *
  type family V c :: *
  searchFor :: K c -> c -> Maybe (V c)

-- | Deposits grouping by day
newtype Deposits = Deposits { unDeposits :: HM.HashMap DayH Deposit }
  deriving (Show, Eq)

instance Semigroup Deposits where
  ds1 <> ds2 = Deposits $ HM.unionWith (<>) (coerce ds1) (coerce ds2)

instance Monoid Deposits where
  mempty = Deposits $ HM.empty
  mappend = (<>)

-- | Interests grouping by day
newtype Interests = Interests { unInterests :: HM.HashMap DayH Interest }
  deriving (Show, Eq)

instance Semigroup Interests where
  ds1 <> ds2 = Interests $ HM.unionWith (<>) (coerce ds1) (coerce ds2)

instance Monoid Interests where
  mempty = Interests $ HM.empty
  mappend = (<>)

instance SearchFor Interests where
  type K Interests = DayH
  type V Interests = Interest
  -- searchFor :: K c -> c -> Maybe (V c)
  searchFor k ints = HM.lookup k (coerce ints)

class HasDeposits a where
  deposits :: a -> Deposits

instance HasDeposits (HM.HashMap DayH Deposit) where
  deposits = Deposits

instance (HasDay d) => HasDeposits (d, Money) where
  deposits (d, m) = deposits $ HM.singleton (day d) (deposit m)

instance SearchFor Deposits where
  type K Deposits = DayH
  type V Deposits = Deposit
  -- searchFor :: K c -> c -> Maybe (V c)
  searchFor k deps = HM.lookup k (coerce deps)

class HasInterests a where
  interests :: a -> Interests

instance HasInterests (HM.HashMap DayH Interest) where
  interests = Interests

instance (HasDay d) => HasInterests (d, Interest) where
  interests (d, m) = interests $ HM.singleton (day d) m

depositsStore :: Day -> Day -> Money -> Deposits
depositsStore sday eday weekly = foldMap (deposits) ls
  where
    nDays :: Integer
    nDays = diffDays eday sday
    ls :: [(Day, Money)]
    ls = do
      i <- [0, 7 .. nDays]
      return (addDays i sday, weekly)

interestsStore :: Day -> Day -> [Rate] -> Interests
interestsStore sday eday rates = foldMap (interests) ls
  where
    -- nDays :: Integer
    -- nDays = diffDays eday sday
    (sgYYYY, sgMM, sgDD) = toGregorian sday
    (egYYYY, _, _) = toGregorian eday
    ls :: [(Day, Interest)]
    ls = zipWith (\ d r -> (d, interest (r :: Rate))) [calDay (yyyy, sgMM, sgDD) | yyyy <- [sgYYYY .. egYYYY]] rates
    -- ls = zipWith
    --      (\m r -> (addDays m sday, interest (r :: Rate)))
    --      [0, 365 .. nDays]
    --      rates -- interest rates changes

type InterestSF = forall m cl . (MonadIO m, Clock m cl) => ClSF m (Daily cl) Day Interest
type DepositSF = forall m cl . (MonadIO m, Clock m cl) => ClSF m (Daily cl) Day Deposit
type MoneySF = forall m cl . (MonadIO m, Clock m cl) => ClSF m (Daily cl) Day (Sum Money)

interestS :: Interests -> InterestSF
interestS ints = feedback mempty (proc pr@(d, int) -> do
                                     let
                                       findI :: Day -> Maybe Interest
                                       findI (day -> d) = searchFor d ints

                                       int' :: Interest
                                       int' = case findI d of
                                         Just i -> i
                                         _ -> int
                                     -- arrMCl (liftIO . print) -< pr
                                     returnA -< (int, int')
                                 )

depositS :: Deposits -> DepositSF
depositS deps = feedback mempty (proc pr@(d, dep) -> do
                                    let
                                      findD :: Day -> Maybe Deposit
                                      findD (day -> d) = searchFor d deps

                                      dep' :: Deposit
                                      dep' = case findD d of
                                        Just d -> d
                                        _ -> mempty
                                    -- arrMCl (liftIO . print) -< pr
                                    returnA -< (dep, dep')
                                )

type CompoundFn = Day -> Bool
type AdjustRateFn = forall a . (Floating a) => a -> a

moneyS :: Day
       -> Day
       -> Sum Money
       -> DepositSF
       -> InterestSF
       -> CompoundFn
       -> AdjustRateFn
       -> MoneySF
moneyS sday eday initAccount depositSf interestSf compoundFn adjustRateFn = proc d -> do
  dp <- depositSf -< d
  ir <- interestSf -< d
  account <- (feedback initAccount (proc pr@((d, dp, ir), account) -> do
                                       nCount <- count -< ()
                                       -- (arrMCl $ liftIO . print) -< (nCount, diffDays eday sday)
                                       let
                                         compound :: Sum Money -> Interest -> Day -> Sum Money
                                         compound acc (rate -> Rate r) (compoundFn -> True) = (coerce (coerce (1 + (adjustRateFn r) / 100) <> (coerce acc) :: Product Money) :: Sum Money)
                                         compound acc _ _ = acc

                                         account' :: Sum Money
                                         account' = if nCount <= (diffDays eday sday)
                                           then compound (account) ir d <> Sum (money dp)
                                           else account

                                       arrMCl (\ pp@(n, pr@((d, dp, ir), _), account, account') -> do
                                                  when
                                                    (n <= (diffDays eday sday) && abs (account' - account) > Sum 0.0001)
                                                    (liftIO . print $ (d, dp, (\case
                                                                                  Rate x -> adjustRateFn x
                                                                                  _ -> 0.0
                                                                              ) (rate ir), account, account'))
                                              ) -< (nCount, pr, account, account')
                                       returnA -< (account, account')
                                   )
             ) -< (d, dp, ir)
  -- arrMCl (liftIO . print) -< account
  returnA -< account

simul :: (HasDay simDay) =>
        simDay -- ^ day
     -> (forall m cl . (MonadIO m, Clock m cl) => ClSF m (Daily cl) Day ())
     -> Freq
     -> IO ()
simul simDay clsf 0 = flow $ (tagS >>> clsf) @@ (clock (Busy) (calDay simDay))
simul simDay clsf ((1000 <) -> True ) = flow $ (tagS >>> clsf) @@ (clock (StdinClock) (calDay simDay))
simul simDay clsf (someNatVal -> Just (SomeNat (_ :: Proxy n))) = flow $ (tagS >>> clsf) @@ (clock (waitClock :: Millisecond n) (calDay simDay))
simul simDay clsf _ = flow $ (tagS >>> clsf) @@ (clock (StdinClock) (calDay simDay))

projectClient
  :: Money
  -> Day
  -> Day
  -> Money
  -> [Savings.Clock.Daily.Rate]
  -> CompoundFn
  -> AdjustRateFn
  -> Savings.Clock.Daily.Freq
  -> IO ()
projectClient
  iamount
  sday
  eday
  monthly
  annualRates
  compoundFn
  adjustRateFn
  freq
  = simul
    sday
    (moneyS
      sday
      eday
      (Sum iamount)
      (depositS $ depositsStore sday eday monthly)
      (interestS $ interestsStore sday eday annualRates)
      (compoundFn)
      (adjustRateFn)
      >>> (arrMCl $ \_ -> liftIO . return $ ())
    )
    freq

-- test :: IO ()
test weeklys rates compounds syear years = projectClient 0 (calDay (syear,1,1)) (calDay (syear+years,1,1)) (weeklys) rates (\d -> case toGregorian d of { (_,mm,dd) | (dd `elem` ([1]::[Int]) && mm `elem` (drop 1 [0, (div 12 compounds) .. 12])) -> True | otherwise -> False; _ -> False }) (/(fromRational . toRational $ compounds)) 0
