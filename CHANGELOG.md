# Change log

frp-savings uses [Semantic Versioning][].
The change log is available through the [releases on GitHub][].

[Semantic Versioning]: http://semver.org/spec/v2.0.0.html
[releases on GitHub]: https://github.com/smunix/frp-savings/releases
